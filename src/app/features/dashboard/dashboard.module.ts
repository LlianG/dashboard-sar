import {NgModule} from '@angular/core';

import {routing} from './dashboard.routing';
import { SharedModule } from '@app/shared/shared.module';
import { ChartJsModule } from '@app/shared/graphs/chart-js/chart-js.module';

@NgModule({
  imports: [
    SharedModule,
    routing,
    ChartJsModule,
  ],
  declarations: [],
  providers: [],
})
export class DashboardModule {

}
