import { Component, OnInit, OnDestroy } from '@angular/core';
import { JsonApiService } from '@app/core/services';
import { Chart } from 'chart.js';

@Component({
  selector: 'sa-pie',
  templateUrl: './pie.component.html',
  styleUrls: ['./pie.component.css']
})
export class PieComponent implements OnInit, OnDestroy {
  
  public chartjsData: any;
  public pieChartOptions = {
    responsive: true,
    legend: {
      position: 'bottom',
      display:false
    }
  };
  public pieChartLabels= ['Camila', 'Mathew', 'Jhon', 'Luis', 'Cristian'];
  public pieChartData = [1600, 1600, 1600, 1600, 1600];
  public myPieChart:any [];
  constructor(private jsonApiService: JsonApiService) {}

  ngOnInit() {
      this.iniciar_chart()
  }
  iniciar_chart(){
    this.myPieChart = new Chart('pie', {
      type: 'pie',
      data: {
        labels:this.pieChartLabels,
        datasets:[{
            data: this.pieChartData,
            backgroundColor: [
              '#0b61a4',
              '#7992a3',
              '#54a74d',
              '#7f4da7',
              '#a74d68'
          ]
        }]
      },
      
      options: this.pieChartOptions
  });
  }
  ngOnDestroy(){}

}
