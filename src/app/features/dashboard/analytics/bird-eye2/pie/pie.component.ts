import { Component, OnInit, OnDestroy } from '@angular/core';
import { JsonApiService } from '@app/core/services';
import { Chart } from 'chart.js';

@Component({
  selector: 'sa-pie2',
  templateUrl: './pie.component.html',
  styleUrls: ['./pie.component.css']
})
export class PieComponent2 implements OnInit, OnDestroy {
  
  public chartjsData: any;
  public pieChartOptions = {
    responsive: true,
    legend: {
      position: 'bottom',
      display:false
    }
  };
  public pieChartLabels= ['Enero', 'Febrero', 'Marzo', 'Abril', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
  public pieChartData = [1600, 1600, 1600, 1600, 1600, 1600, 1600, 1600, 1600, 1600, 1600, 1600];
  public myPieChart2:any [];
  constructor(private jsonApiService: JsonApiService) {}

  ngOnInit() {
      this.iniciar_chart()
  }
  iniciar_chart(){
    this.myPieChart2 = new Chart('pie2', {
      type: 'pie',
      data: {
        labels:this.pieChartLabels,
        datasets:[{
            data: this.pieChartData,
            backgroundColor: [
              '#0b61a4',
              '#7992a3',
              '#54a74d',
              '#7f4da7',
              '#a74d68',
              '#a46c0c',
              '#d5ae05',
              '#680676',
              '#0ab8c6',
              '#52b254',
              '#4464a7',
              '#cc1f1f'
          ]
        }]
      },
      
      options: this.pieChartOptions
  });
  }
  ngOnDestroy(){}

}
