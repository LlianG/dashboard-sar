import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BirdEye2Component } from './bird-eye2.component';

describe('BirdEye2Component', () => {
  let component: BirdEye2Component;
  let fixture: ComponentFixture<BirdEye2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BirdEye2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BirdEye2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
